#!/usr/bin/env  python
# -*- coding: utf-8 -*-
try:
    import zmq
    import argparse
    import signal
    import time
    import datetime
    import logging
    import yaml
    import psycopg2
    import pytz
except Exception as e:
    fail(e)


def fail(e):
    if hasattr(e,'message') and e.message:
        print 'Server: '+e.message
    else:
        print 'Server: '+e
    #raise
    exit(1)


def handler(sig, frame):
    print SIGNAMES[sig] + " received. We'll have to shut up shop..."
    exit(0)


# SOME HACK TO GET SIGNAL NAMES
SIGNAMES = dict((k, v) for v, k in reversed(sorted(signal.__dict__.items()))
                if v.startswith('SIG') and not v.startswith('SIG_'))


signal.signal(signal.SIGINT, handler)
signal.signal(signal.SIGTERM, handler)

logging.basicConfig(format="%(message)s", level=logging.DEBUG)


def getSettings(fname):
    with open(fname) as settings:
        return yaml.load(settings)


def createTable(cur):
    sql = '''CREATE TABLE "public"."messages" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "tstamp" timestamp(6) WITH TIME ZONE NOT NULL,
    "ip" inet NOT NULL,
    "port" int4,
    "message" text COLLATE "default"
    )
    WITH (OIDS=FALSE);
    ALTER TABLE "public"."messages" OWNER TO "test_user";'''
    cur.execute(sql)


def insertMessage(cur, data):
    sql = "INSERT INTO messages (tstamp,ip,port,message) VALUES(TIMESTAMP %s,%s,%s,%s)"
    cur.execute(sql, data)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Zeromq server')
    parser.add_argument('-s', '--settings',
                        help='settings file name', type=str)

    try:
        args = parser.parse_args()
        if not args.settings:
            parser.print_help()
            exit(0)
        settings = getSettings(args.settings)

        context = zmq.Context()
        socket = context.socket(zmq.REP)
        socket.bind('tcp://%s:%s' %
                    (settings['server']['host'], settings['server']['port']))

        conn_string = "dbname='%s' user='%s' host='%s' port='%s' password='%s' "
        with psycopg2.connect(conn_string % (settings['db']['dbname'],
                                             settings['db']['user'],
                                             settings['db']['host'],
                                             settings['db']['port'],
                                             settings['db']['password'])) as conn:
            conn.autocommit = True
            cur = conn.cursor()
            cur.execute(
                """SELECT * FROM information_schema.tables WHERE table_name=%s""", ('messages',))
            if not bool(cur.rowcount):
                createTable(cur)

            while True:
                message = socket.recv()
                try:
                    data = (datetime.datetime.now(tz=pytz.utc), '127.0.0.1',
                            '80', message)
                    insertMessage(cur, data)
                    #tprint('Worker received %s from %s' % (message, ident))
                    logging.info("%s, %s, %s, %s" %
                                 (data[0], data[1], data[2], data[3]))
                except:
                    socket.send("ERROR")
                    raise

                socket.send("OK")
    # CATCHING ANY ROUTINE PROBLEM
    except psycopg2.OperationalError as e:
        fail("DB connection error")
    except Exception as e:
        try:
            socket.close()
            context.term()
        except:
            pass
        fail(e)
