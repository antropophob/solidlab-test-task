function mytest {
    "$@"
    local status=$?
    if [ $status -ne 0 ]; then
        echo "error with $1" >&2
    fi
    return $status
}


mytest ./client.py -c 10 -a localhost -p 43000 &
mytest ./server.py -s server_settings.yml