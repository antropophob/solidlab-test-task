#!/usr/bin/env  python
# -*- coding: utf-8 -*-

try:
    import os
    import argparse
    import zmq
    import string
    import random
    import time

except Exception as e:
    fail(e)

MESSAGE_LEN = 20
REQUEST_TIMEOUT = 1  # server request timeout in seconds


def fail(e):
    if hasattr(e,'message') and e.message:
        print 'Client: '+e.message
    else:
        print 'Client: '+e
    #raise
    exit(1)


def generateMessage(n):
    return ''.join(random.SystemRandom().choice(string.ascii_uppercase) for i in range(n))


def connect(host, port):
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.setsockopt(zmq.LINGER, 0)
    socket.connect("tcp://%s:%s" % (host, port))
    return socket, context

if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        description='Sends some messages through zeromq')
    parser.add_argument('-c', '--count', help='message count', type=int)
    parser.add_argument('-a', '--address', help='server host', type=str)
    parser.add_argument('-p', '--port', help='server port', type=int)

    try:
        args = parser.parse_args()
        if not (args.address and args.port and args.count):
            parser.print_help()
            exit(0)
        socket, context = connect(args.address, args.port)
        poller = zmq.Poller()
        poller.register(socket, zmq.POLLIN)


        for i in range(args.count):
            # print "Sending message %s"%str(i+1)
            socket.send(generateMessage(MESSAGE_LEN))
            if poller.poll(REQUEST_TIMEOUT * 1000):
                message = socket.recv()
            else:
                raise IOError('Connection timeout')
            if not message == 'OK':
                raise IOError(message + ' received from server')
            time.sleep(1)

    except Exception as e:
        fail(e)

    socket.close()
    context.term()
    exit(0)
